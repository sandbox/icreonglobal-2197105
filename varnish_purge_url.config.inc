<?php
/**
 * @file
 * Configuration form for Varnish server.
 */

/**
 * Implements varnish_purge_url_config_form() to generate config form.
 */
function varnish_purge_url_config_form($form, &$form_state) {
  $server_ip = isset($form_state['values']['varnish_server_config']['server_ip']) ? $form_state['values']['varnish_server_config']['server_ip'] : variable_get('server_ip', 0);
  $server_secret_key = isset($form_state['values']['varnish_server_config']['server_secret_key']) ? $form_state['values']['varnish_server_config']['server_secret_key'] : variable_get('server_secret_key', 0);
  $server_version = isset($form_state['values']['varnish_server_config']['server_version']) ? $form_state['values']['varnish_server_config']['server_version'] : variable_get('server_version', 0);
  $url_to_purge = isset($form_state['values']['varnish_server_config']['url_to_purge']) ? $form_state['values']['varnish_server_config']['url_to_purge'] : variable_get('url_to_purge', 0);
  $path = drupal_get_path('module', 'varnish_purge_url');
  $form['#tree'] = TRUE;
  $form['varnish_server_config'] = array(
    '#type' => 'fieldset',
    '#title' => 'Server Information',
    '#prefix' => '<div id="varnish-server-config-fieldset-wrapper">',
    '#suffix' => '</div>',
  );
  // Decide whether or not to flush caches on cron runs.
  $form['varnish_server_config']['varnish_purge_url_status'] = array(
    '#type' => 'radios',
    '#title' => t('Varnish Purge URL Feature?'),
    '#options' => array(
      0 => t('Disabled'),
      1 => t('Enabled'),
    ),
    '#default_value' => variable_get('varnish_purge_url_status', 1),
    '#description' => t("In case of disabled, you couldn't purge URL cache from Varnish using this module."),
  );
  $form['varnish_server_config']['server_ip'] = array(
    '#type' => 'textfield',
    '#title' => t('Varnish Control Terminal'),
    '#description' => t('Please enter varnish server IP or hostname with port that varnish runs on (eg. 127.0.0.1:6082). You can add multiple servers with comma separated'),
    '#default_value' => $server_ip,
    '#required' => TRUE,
  );
  $form['varnish_server_config']['server_secret_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Secret Key'),
    '#description' => t('Please enter secret key of Varnish server. You can add multiple keys with comma separated.'),
    '#default_value' => $server_secret_key,
    '#required' => TRUE,
  );
  $form['varnish_server_config']['server_version'] = array(
    '#type' => 'select',
    '#title' => t('Version'),
    '#options' => array('3' => t('3.x'), '2' => t('2.x')),
    '#description' => t('Please select version of Varnish server'),
    '#default_value' => $server_version,
    '#required' => TRUE,

  );
  $options = array(
    '1' => 1,
    '2' => 2,
    '3' => 3,
    '4' => 4,
    '5' => 5,
    '6' => 6,
    '7' => 7,
    '8' => 8,
    '9' => 9,
    '10' => 10,
  );
  $form['varnish_server_config']['url_to_purge'] = array(
    '#type' => 'select',
    '#title' => t('Max. number of URLs to purge'),
    '#description' => t('Please select max. number of URLs you wants to purge together.'),
    '#options' => $options,
    '#required' => TRUE,
    '#default_value' => $url_to_purge,

  );
  $html = "";
  $img = '';
  if (!empty($server_ip)) {
    $server_array = explode(',', $server_ip);
    $servers = array_map('trim', $server_array);
    $secret_keys_array = explode(',', $server_secret_key);
    $secret_keys = array_map('trim', $secret_keys_array);
    $k = 0;
    foreach ($servers as $server) {
      $servers_keys_array[$server] = $secret_keys[$k];
      $k++;
    }

    $html = '<ul style="padding-left:0;margin-left:0;">';
    foreach ($servers_keys_array as $server_terminal => $server_secret_keys) {
      $output = varnish_purge_url_check_socket_connection($server_terminal, $server_secret_keys, $server_version);
      list($server, $port) = explode(':', $server_terminal);
      if ($output[$server_terminal]['status'] == 200) {
        $img = "<img src = '/$path/images/check.jpg'>";
        $html .= '<li style = "list-style:none;">' . '<span style="display:inline-block;float:left;margin-right:5px;">' . $img . '</span> The Varnish control Terminal is responding at ' . $server . ' on port ' . $port . '</li>';
      }
      else {
        $img = "<img src = '/$path/images/cross.jpg'>";
        $html .= '<li style = "list-style:none";>' . '<span style="display:inline-block;float:left;margin-right:5px;">' . $img . '</span> The Varnish control Terminal is not responding at ' . $server . ' on port ' . $port . '</li>';
      }
    }
    $html .= "</ul>";
  }
  $form['varnish_server_config']['connection_status'] = array(
    '#markup' => '<b>Varnish Connection Status :</b>' . $html,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;

}

/**
 * Implements hook_form_validate().
 */
function varnish_purge_url_config_form_validate($form, &$form_state) {
  $ips_value = $form_state['values']['varnish_server_config']['server_ip'];
  $varnish_version = $form_state['values']['varnish_server_config']['server_version'];
  $secret_keys_all = $form_state['values']['varnish_server_config']['server_secret_key'];

  // Checking for duplicate IP values.
  $terminal_array = explode(',', $ips_value);
  // Trim the IP values.
  $terminals = array_map('trim', $terminal_array);
  $secret_keys_array = explode(',', $secret_keys_all);
  // Trim the secret keys values.
  $secret_keys = array_map('trim', $secret_keys_array);
  // Checking number of ip and keys are values.
  if (count($terminals) != count($secret_keys)) {
    form_set_error('varnish_server_config][server_ip', t("Total IP and Total Secret keys are not matched."));
  }
  $counts_terminals = array_count_values($terminals);
  foreach ($counts_terminals as $counts_terminal) {
    if ($counts_terminal > 1) {
      form_set_error('varnish_server_config][server_ip', t("Duplicate IP with port exist."));
    }
  }

  // Checking for duplicate secrete key.
  $counts_keys = array_count_values($secret_keys);
  foreach ($counts_keys as $counts_key) {
    if ($counts_key > 1) {
      form_set_error('varnish_server_config][server_secret_key', t("Duplicate secret key exist."));
    }
  }
  $ip_keys_array = array();
  $k = 0;
  foreach ($terminals as $terminal) {
    $ip_keys_array[$terminal] = $secret_keys[$k];
    $k++;
  }
  // Checking enter all IPs.
  if (count($ip_keys_array) > 0) {
    foreach ($ip_keys_array as $ip_terminal => $ip_secret_keys) {
      $port_value = strpos($ip_terminal, ':');
      if (!$port_value) {
        form_set_error('varnish_server_config][server_ip', t("You need to enter server IP %ip_terminal with port", array('%ip_terminal' => $ip_terminal)));
      }
      if (strpos($ip_terminal, ':')) {
        list($server, $port) = explode(':', $ip_terminal);
        $timeout = 100;
        $seconds = (int) ($timeout / 1000);
        $microseconds = (int) ($timeout % 1000 * 1000);
        if ($port != "" && $server != "") {
          // Open a socket.
          $client = socket_create(AF_INET, SOCK_STREAM, getprotobyname('tcp'));
          socket_set_option($client, SOL_SOCKET, SO_SNDTIMEO, array('sec' => $seconds, 'usec' => $microseconds));
          socket_set_option($client, SOL_SOCKET, SO_RCVTIMEO, array('sec' => $seconds, 'usec' => $microseconds));
          if (@!socket_connect($client, $server, $port)) {
            watchdog('varnish_purge_url', 'Unable to connect to server socket !server:!port: %error', array(
                    '!server' => $server,
                    '!port' => $port,
                     '%error' => socket_strerror(socket_last_error($client)),
                   ), WATCHDOG_ERROR);
            form_set_error('varnish_server_config][server_ip', t('The Varnish control terminal is not responding at %server on port %port', array('%server' => $server, '%port' => $port)));
          }
          elseif (floatval($varnish_version) > 2.0) {
            $status = varnish_purge_read_socket($client);
            // Authentication.
            if ($status['code'] == 107) {
              // Validating secret keys.
              if ($ip_secret_keys) {
                $challenge = substr($status['msg'], 0, 32);
                $pack = $challenge . "\x0A" . $ip_secret_keys . "\x0A" . $challenge . "\x0A";
                $key = hash('sha256', $pack);
                socket_write($client, "auth $key\n");
                $status = varnish_purge_read_socket($client);
                if ($status['code'] != 200) {
                  form_set_error('varnish_server_config][server_secret_key', t('The Varnish control terminal is not responding at %server on port %port', array('%server' => $server, '%port' => $port)));
                }
              }
            } // End of check status code.
          }
          // Closing the socket connection
          // socket_shutdown($client, 2);
          socket_close($client);
        }
      }
    } // End of foerach.
  } // End of if.
}

/**
 * Implements hook_form_submit().
 */
function varnish_purge_url_config_form_submit($form, &$form_state) {
  $server_information = $form_state['values']['varnish_server_config'];
  $varnish_purge_url_status = $server_information['varnish_purge_url_status'];
  $server_ip = $server_information['server_ip'];
  $server_secret_key = $server_information['server_secret_key'];
  $server_version = $server_information['server_version'];
  $no_of_url = $server_information['url_to_purge'];

  variable_set('varnish_purge_url_status', $varnish_purge_url_status);
  variable_set('url_to_purge', $no_of_url);
  variable_set('server_ip', $server_ip);
  variable_set('server_secret_key', $server_secret_key);
  variable_set('server_version', $server_version);
  drupal_set_message(t('Your configuration has been saved'));
}

/**
 * Function varnish_purge_url_check_socket_connection().
 */
function varnish_purge_url_check_socket_connection($terminal_ip, $secret_keys, $server_varnish_version) {
  $ret = array();
  $status = '';
  list($server, $port) = explode(':', $terminal_ip);
  $timeout = 100;
  $seconds = (int) ($timeout / 1000);
  $microseconds = (int) ($timeout % 1000 * 1000);
  if ($port != "" && $server != "") {
    // Open a socket.
    $client = socket_create(AF_INET, SOCK_STREAM, getprotobyname('tcp'));
    socket_set_option($client, SOL_SOCKET, SO_SNDTIMEO, array('sec' => $seconds, 'usec' => $microseconds));
    socket_set_option($client, SOL_SOCKET, SO_RCVTIMEO, array('sec' => $seconds, 'usec' => $microseconds));
    if (@!socket_connect($client, $server, $port)) {
      watchdog('varnish_purge_url', 'Unable to connect to server socket !server:!port: %error', array(
              '!server' => $server,
              '!port' => $port,
               '%error' => socket_strerror(socket_last_error($client)),
             ), WATCHDOG_ERROR);
      $ret[$terminal_ip['key']] = FALSE;
    }
    elseif (floatval($server_varnish_version) > 2.0) {
      $status = varnish_purge_read_socket($client);
      // Authentication.
      if ($status['code'] == 107) {
        // Validating secret keys.
        if ($secret_keys) {
          $challenge = substr($status['msg'], 0, 32);
          $pack = $challenge . "\x0A" . $secret_keys . "\x0A" . $challenge . "\x0A";
          $key = hash('sha256', $pack);
          socket_write($client, "auth $key\n");
          $status = varnish_purge_read_socket($client);
        }
      } // End of check status code.
    }
    // Closing the socket connection
    // socket_shutdown($client, 2);
    socket_close($client);
    if (is_array($status)) {
      $ret[$terminal_ip]['status'] = $status['code'];
    }
  }
  return $ret;
}
