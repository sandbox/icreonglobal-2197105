This module allows users to purge varnish cache of specific URLs through an
interface in backend. It connects to varnish through socket on listning
IP and port.

## What does it do?
The main function of the varnish_purge_url.module is to dynamically purge
URLs from the Varnish cache. 

## Installation
First of all, you need to have Varnish installed on your server.
For more information on how to do this, please see:
http://www.varnish-cache.org/

Once you have Varnish working, use the module you need to do the following:

* Enable the module.

* Go to admin/config/development/varnish-purge-url/settings and
configure your server configurations of Varnish appropriately. 
  
* You can find secret key of your Varnish server at /etc/varnish/secret

* You can checkout Varnish version by using varnishd -V command.

* If your application is cached by multiple varish servers then you can add
multiple varnish servers with comma separated. In same way, you can add multiple
varnish servers secret keys.

* You can checkout your varnish server configuration at same page.If it
couldn't make connection then you need to checkout varnish servers
logs for more details.

* If you are not using SYSLOG then you can comment syslog() functions
in varnish_purge.module file.

* If same varnish server is configured to serve multiple sites then you can
also use hostname in varnish_page_purge_url() function to purge cache
from specific host.

* You can enable/disable varnish purge URLs feature from module settings.

* You need to enter relative URLs instead of absolute URLs to purge.
For example, you want to flush http://www.abc.com/about-us page
cache so you just need to use about-us url in module.
